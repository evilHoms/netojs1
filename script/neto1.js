'use strict';

var name,
    price;

name = `Телепорт бытовой VZHIH-101`;
price = 10000;

console.clear();
console.log(`В наличии имеется: "${name}"`);
console.log(`Стоимость товара ${price} Q`);

var discountPersent = 0.1,
    totalDiscount = 0,
    numberOfItems = 2,
    totalPrice = 0;

totalDiscount = price * numberOfItems * discountPersent;
totalPrice = price * numberOfItems - totalDiscount;

console.log(`Цена покупки составит ${totalPrice} Q`);

var cash = 52334224,
    teleportPrice = 6500,
    cashLeft = 0,
    numberOfTeleports = 0;

cashLeft = cash % teleportPrice;
numberOfTeleports = Math.floor(cash / teleportPrice);

console.log(`Мы можем закупить ${numberOfTeleports} единиц товара, после закупки на счету останется ${cashLeft} Q`);